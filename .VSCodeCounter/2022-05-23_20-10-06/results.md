# Summary

Date : 2022-05-23 20:10:06

Directory c:\Users\wh1pe\Desktop\MAIN

Total : 44 files,  2298 codes, 263 comments, 332 blanks, all 2893 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 9 | 1,353 | 33 | 122 | 1,508 |
| C++ | 11 | 405 | 80 | 132 | 617 |
| JSON | 3 | 180 | 0 | 3 | 183 |
| XML | 9 | 124 | 47 | 12 | 183 |
| Groovy | 3 | 88 | 3 | 22 | 113 |
| HTML | 1 | 80 | 18 | 7 | 105 |
| YAML | 2 | 23 | 81 | 17 | 121 |
| Markdown | 2 | 13 | 0 | 9 | 22 |
| CMake | 1 | 12 | 0 | 4 | 16 |
| Swift | 1 | 12 | 0 | 2 | 14 |
| Properties | 2 | 8 | 1 | 2 | 11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 44 | 2,298 | 263 | 332 | 2,893 |
| flutter_project | 44 | 2,298 | 263 | 332 | 2,893 |
| flutter_project\android | 12 | 159 | 49 | 34 | 242 |
| flutter_project\android\app | 8 | 116 | 48 | 23 | 187 |
| flutter_project\android\app\src | 7 | 63 | 45 | 10 | 118 |
| flutter_project\android\app\src\debug | 1 | 4 | 3 | 1 | 8 |
| flutter_project\android\app\src\main | 5 | 55 | 39 | 8 | 102 |
| flutter_project\android\app\src\main\res | 4 | 26 | 32 | 6 | 64 |
| flutter_project\android\app\src\main\res\drawable | 1 | 4 | 7 | 2 | 13 |
| flutter_project\android\app\src\main\res\drawable-v21 | 1 | 4 | 7 | 2 | 13 |
| flutter_project\android\app\src\main\res\values | 1 | 9 | 9 | 1 | 19 |
| flutter_project\android\app\src\main\res\values-night | 1 | 9 | 9 | 1 | 19 |
| flutter_project\android\app\src\profile | 1 | 4 | 3 | 1 | 8 |
| flutter_project\android\gradle | 1 | 5 | 1 | 1 | 7 |
| flutter_project\android\gradle\wrapper | 1 | 5 | 1 | 1 | 7 |
| flutter_project\ios | 7 | 222 | 2 | 9 | 233 |
| flutter_project\ios\Runner | 7 | 222 | 2 | 9 | 233 |
| flutter_project\ios\Runner\Assets.xcassets | 3 | 148 | 0 | 4 | 152 |
| flutter_project\ios\Runner\Assets.xcassets\AppIcon.appiconset | 1 | 122 | 0 | 1 | 123 |
| flutter_project\ios\Runner\Assets.xcassets\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| flutter_project\ios\Runner\Base.lproj | 2 | 61 | 2 | 2 | 65 |
| flutter_project\lib | 8 | 1,336 | 23 | 115 | 1,474 |
| flutter_project\test | 1 | 17 | 10 | 7 | 34 |
| flutter_project\web | 2 | 115 | 18 | 8 | 141 |
| flutter_project\windows | 11 | 416 | 80 | 135 | 631 |
| flutter_project\windows\flutter | 3 | 20 | 9 | 15 | 44 |
| flutter_project\windows\runner | 8 | 396 | 71 | 120 | 587 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)