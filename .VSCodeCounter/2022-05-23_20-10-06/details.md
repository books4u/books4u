# Details

Date : 2022-05-23 20:10:06

Directory c:\Users\wh1pe\Desktop\MAIN

Total : 44 files,  2298 codes, 263 comments, 332 blanks, all 2893 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [flutter_project/README.md](/flutter_project/README.md) | Markdown | 10 | 0 | 7 | 17 |
| [flutter_project/analysis_options.yaml](/flutter_project/analysis_options.yaml) | YAML | 3 | 23 | 4 | 30 |
| [flutter_project/android/app/build.gradle](/flutter_project/android/app/build.gradle) | Groovy | 53 | 3 | 13 | 69 |
| [flutter_project/android/app/src/debug/AndroidManifest.xml](/flutter_project/android/app/src/debug/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [flutter_project/android/app/src/main/AndroidManifest.xml](/flutter_project/android/app/src/main/AndroidManifest.xml) | XML | 29 | 7 | 2 | 38 |
| [flutter_project/android/app/src/main/res/drawable-v21/launch_background.xml](/flutter_project/android/app/src/main/res/drawable-v21/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [flutter_project/android/app/src/main/res/drawable/launch_background.xml](/flutter_project/android/app/src/main/res/drawable/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [flutter_project/android/app/src/main/res/values-night/styles.xml](/flutter_project/android/app/src/main/res/values-night/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [flutter_project/android/app/src/main/res/values/styles.xml](/flutter_project/android/app/src/main/res/values/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [flutter_project/android/app/src/profile/AndroidManifest.xml](/flutter_project/android/app/src/profile/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [flutter_project/android/build.gradle](/flutter_project/android/build.gradle) | Groovy | 27 | 0 | 5 | 32 |
| [flutter_project/android/gradle.properties](/flutter_project/android/gradle.properties) | Properties | 3 | 0 | 1 | 4 |
| [flutter_project/android/gradle/wrapper/gradle-wrapper.properties](/flutter_project/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 5 | 1 | 1 | 7 |
| [flutter_project/android/settings.gradle](/flutter_project/android/settings.gradle) | Groovy | 8 | 0 | 4 | 12 |
| [flutter_project/ios/Runner/AppDelegate.swift](/flutter_project/ios/Runner/AppDelegate.swift) | Swift | 12 | 0 | 2 | 14 |
| [flutter_project/ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json](/flutter_project/ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json) | JSON | 122 | 0 | 1 | 123 |
| [flutter_project/ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json](/flutter_project/ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json) | JSON | 23 | 0 | 1 | 24 |
| [flutter_project/ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md](/flutter_project/ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md) | Markdown | 3 | 0 | 2 | 5 |
| [flutter_project/ios/Runner/Base.lproj/LaunchScreen.storyboard](/flutter_project/ios/Runner/Base.lproj/LaunchScreen.storyboard) | XML | 36 | 1 | 1 | 38 |
| [flutter_project/ios/Runner/Base.lproj/Main.storyboard](/flutter_project/ios/Runner/Base.lproj/Main.storyboard) | XML | 25 | 1 | 1 | 27 |
| [flutter_project/ios/Runner/Runner-Bridging-Header.h](/flutter_project/ios/Runner/Runner-Bridging-Header.h) | C++ | 1 | 0 | 1 | 2 |
| [flutter_project/lib/AddBookScreen.dart](/flutter_project/lib/AddBookScreen.dart) | Dart | 266 | 0 | 17 | 283 |
| [flutter_project/lib/bookClass.dart](/flutter_project/lib/bookClass.dart) | Dart | 59 | 8 | 14 | 81 |
| [flutter_project/lib/home.dart](/flutter_project/lib/home.dart) | Dart | 431 | 11 | 31 | 473 |
| [flutter_project/lib/loginScreen.dart](/flutter_project/lib/loginScreen.dart) | Dart | 174 | 0 | 12 | 186 |
| [flutter_project/lib/main.dart](/flutter_project/lib/main.dart) | Dart | 19 | 0 | 5 | 24 |
| [flutter_project/lib/profileClass.dart](/flutter_project/lib/profileClass.dart) | Dart | 34 | 4 | 9 | 47 |
| [flutter_project/lib/registerScreen.dart](/flutter_project/lib/registerScreen.dart) | Dart | 181 | 0 | 13 | 194 |
| [flutter_project/lib/styles.dart](/flutter_project/lib/styles.dart) | Dart | 172 | 0 | 14 | 186 |
| [flutter_project/pubspec.yaml](/flutter_project/pubspec.yaml) | YAML | 20 | 58 | 13 | 91 |
| [flutter_project/test/widget_test.dart](/flutter_project/test/widget_test.dart) | Dart | 17 | 10 | 7 | 34 |
| [flutter_project/web/index.html](/flutter_project/web/index.html) | HTML | 80 | 18 | 7 | 105 |
| [flutter_project/web/manifest.json](/flutter_project/web/manifest.json) | JSON | 35 | 0 | 1 | 36 |
| [flutter_project/windows/flutter/generated_plugin_registrant.cc](/flutter_project/windows/flutter/generated_plugin_registrant.cc) | C++ | 3 | 4 | 5 | 12 |
| [flutter_project/windows/flutter/generated_plugin_registrant.h](/flutter_project/windows/flutter/generated_plugin_registrant.h) | C++ | 5 | 5 | 6 | 16 |
| [flutter_project/windows/flutter/generated_plugins.cmake](/flutter_project/windows/flutter/generated_plugins.cmake) | CMake | 12 | 0 | 4 | 16 |
| [flutter_project/windows/runner/flutter_window.cpp](/flutter_project/windows/runner/flutter_window.cpp) | C++ | 45 | 4 | 13 | 62 |
| [flutter_project/windows/runner/flutter_window.h](/flutter_project/windows/runner/flutter_window.h) | C++ | 20 | 5 | 9 | 34 |
| [flutter_project/windows/runner/main.cpp](/flutter_project/windows/runner/main.cpp) | C++ | 30 | 4 | 10 | 44 |
| [flutter_project/windows/runner/resource.h](/flutter_project/windows/runner/resource.h) | C++ | 9 | 6 | 2 | 17 |
| [flutter_project/windows/runner/utils.cpp](/flutter_project/windows/runner/utils.cpp) | C++ | 53 | 2 | 10 | 65 |
| [flutter_project/windows/runner/utils.h](/flutter_project/windows/runner/utils.h) | C++ | 8 | 6 | 6 | 20 |
| [flutter_project/windows/runner/win32_window.cpp](/flutter_project/windows/runner/win32_window.cpp) | C++ | 183 | 15 | 48 | 246 |
| [flutter_project/windows/runner/win32_window.h](/flutter_project/windows/runner/win32_window.h) | C++ | 48 | 29 | 22 | 99 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)