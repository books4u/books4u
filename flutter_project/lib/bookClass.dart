import 'dart:convert';

import 'package:flutter_project/home.dart';
import 'package:http/http.dart' as http;

Future<List<Book>> fetchBook() async {
  final response =
      await http.get(Uri.parse('http://23.97.221.80/Books/list.php'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    var tagObjsJson = jsonDecode(response.body)['data'] as List;

    List<Book> tagObjs =
        tagObjsJson.map((tagJson) => Book.fromJson(tagJson)).toList();

    return tagObjs;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Nepavyko gauti knygų sąrašo.');
  }
}

Future<List<Book>> fetchMyBooks() async {
  final response =
      await http.post(Uri.parse("http://23.97.221.80/Books/mylist.php"), body: {
    "user": MyBooksPageState.Username,
  });
  print(MyBooksPageState.Username + " \n" + response.body);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    var tagObjsJson = jsonDecode(response.body)['data'] as List;

    List<Book> tagObjs =
        tagObjsJson.map((tagJson) => Book.fromJson(tagJson)).toList();

    return tagObjs;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Nepavyko gauti knygų sąrašo.');
  }
}

class Book {
  final String name;
  final String author;
  final String genre;
  final String isbn;
  final String description;
  final String wear;
  final String user;

  const Book({
    required this.name,
    required this.author,
    required this.genre,
    required this.isbn,
    required this.description,
    required this.wear,
    required this.user,
  });

  factory Book.fromJson(dynamic json) {
    return Book(
      name: json['name'] as String,
      author: json['author'] as String,
      genre: json['genre'] as String,
      isbn: json['isbn'] as String,
      description: json['description'] as String,
      wear: json['wear'] as String,
      user: json['user'] as String,
    );
  }
}
