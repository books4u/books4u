import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_project/registerScreen.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_project/styles.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';

class LoginClass extends StatefulWidget {
  const LoginClass({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LoginClassState();
}

class LoginClassState extends State<LoginClass> {
  static TextEditingController userController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool isMsgVisible = false;
  String msg = "";

  void changeErrVisibility(String code, bool change) {
    if (change) {
      setState(() {
        isMsgVisible = change;
        msg = "*" + code;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          colorScheme: Styles.defaultColorScheme,
        ),
        home: Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text('Books4u',
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  )),
              const SizedBox(height: 40.0),
              _buildUserNameField(userController),
              const SizedBox(height: 20.0),
              _buildPasswordField(passwordController, context),
              const SizedBox(height: 10.0),
              Visibility(
                child: Container(
                  width: 350,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    msg,
                    style: TextStyle(
                      color: Styles.defaultColorScheme.error,
                    ),
                  ),
                  height: 20,
                ),
                visible: isMsgVisible,
              ),
              Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                    _buildLoginButton(
                        context, userController, passwordController),
                    const SizedBox(
                        width: 350,
                        child:
                            Divider(color: Color.fromARGB(159, 131, 129, 129))),
                    _buildRegisterButton(context)
                  ]))
            ],
          ),
        ));
  }

  Center _buildPasswordField(
      TextEditingController passwordController, BuildContext context) {
    return Center(
        child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        obscureText: true,
        enableSuggestions: false,
        autocorrect: false,
        controller: passwordController,
        textAlignVertical: TextAlignVertical.bottom,
        textAlign: TextAlign.left,
        keyboardType: TextInputType.text,
        decoration: Styles.customInputDecorationPassword(),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
      ),
    ));
  }

  Center _buildUserNameField(TextEditingController userController) {
    return Center(
        child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: userController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationUser(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    ));
  }

  Container _buildLoginButton(
      BuildContext context,
      TextEditingController userController,
      TextEditingController passwordController) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
        style: Styles.raisedButtonStyle,
        onPressed: () async {
          try {
            var response = await http.post(
                Uri.parse("http://23.97.221.80/UserAccounts/login.php"),
                body: {
                  "username": userController.text,
                  "password": passwordController.text,
                });
            if (response.body == "1") {
              SaveUsername(userController.text);

              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const App(loggedIn: "1")),
              );
            } else {
              changeErrVisibility(response.body.toString(), true);
            }
          } catch (error) {
            changeErrVisibility("Nepavyko prisijungti prie serverio.", true);
          }
        },
        child: Text(
          'Prisijungti',
          style: Styles.customButtonTextStyle(),
        ),
      ),
    );
  }

  Container _buildRegisterButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
        style: Styles.raisedButtonStyle,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const RegisterClass()),
          );
        },
        child: Text(
          'Registruotis',
          style: Styles.customButtonTextStyle(),
        ),
      ),
    );
  }

  Future<void> SaveUsername(String user) async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('Username', user);
    });
  }
}
