import 'dart:convert';

class Book {
  String name = "";
  String iSBN = "";
  String author = "";
  String genre = "";
  String desc = "";
  String wear = "";

  Book(String nname, String iisbn, String aauthor, String ggenre, String ddescr,
      String wwear) {
    name = nname;
    iSBN = iisbn;
    author = aauthor;
    genre = ggenre;
    desc = ddescr;
    wear = wwear;
  }
}
