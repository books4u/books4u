import 'dart:convert';

import 'package:flutter_project/home.dart';
import 'package:flutter_project/loginScreen.dart';
import 'package:http/http.dart' as http;

Future<List<Profile>> fetchProfile() async {
  final response = await http
      .post(Uri.parse('http://23.97.221.80/UserAccounts/info.php'), body: {
    "user": LoginClassState.userController.text,
  });
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    var tagObjsJson = jsonDecode(response.body)['data'] as List;

    List<Profile> tagObjs =
        tagObjsJson.map((tagJson) => Profile.fromJson(tagJson)).toList();

    return tagObjs;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Nepavyko gauti knygų sąrašo.');
  }
}

class Profile {
  final String name;
  final String role;
  final String description;

  const Profile({
    required this.name,
    required this.role,
    required this.description,
  });

  factory Profile.fromJson(dynamic json) {
    return Profile(
        name: json['name'] as String,
        role: json['role'] as String,
        description: json['description'] as String);
  }
}
