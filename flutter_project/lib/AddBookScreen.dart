import 'package:flutter/material.dart';
import 'package:flutter_project/home.dart';
import 'package:http/http.dart' as http;
import 'styles.dart';

class AddBookClass extends StatefulWidget {
  const AddBookClass({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddBookClassState();
}

class _AddBookClassState extends State<AddBookClass> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController authorController = TextEditingController();
  final TextEditingController genreController = TextEditingController();
  final TextEditingController isbnController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController wearController = TextEditingController();
  bool isMsgVisible = false;
  String msg = "";

  void changeErrVisibility(String code, bool change) {
    if (change) {
      setState(() {
        isMsgVisible = change;
        msg = "*" + code;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          colorScheme: Styles.defaultColorScheme,
        ),
        home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: const Color.fromARGB(0, 0, 0, 0),
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back,
                  color: Styles.defaultColorScheme.secondary),
              onPressed: () => Navigator.pop(context, false),
            ),
          ),
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text('Pridėti knygą',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    )),
                const SizedBox(height: 40.0),
                _buildNameField(nameController),
                const SizedBox(height: 10.0),
                _buildAuthorField(authorController),
                const SizedBox(height: 10.0),
                _buildIsbnField(isbnController),
                const SizedBox(height: 10.0),
                _buildWearField(wearController),
                const SizedBox(height: 10.0),
                _buildGenreField(genreController),
                const SizedBox(height: 10.0),
                _buildDescriptionField(descriptionController),
                const SizedBox(height: 10.0),
                Visibility(
                  child: Container(
                    width: 350,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      msg,
                      style: TextStyle(
                        color: Styles.defaultColorScheme.error,
                      ),
                    ),
                    height: 20,
                  ),
                  visible: isMsgVisible,
                ),
                Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      const SizedBox(
                          width: 350,
                          child: Divider(
                              color: Color.fromARGB(159, 131, 129, 129))),
                      _buildAddButton(context, MyBooksPageState.Username)
                    ]))
              ],
            ),
          ),
        ));
  }

  Container _buildNameField(TextEditingController NameController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: NameController,
        textAlignVertical: TextAlignVertical.bottom,
        textAlign: TextAlign.left,
        keyboardType: TextInputType.text,
        decoration: Styles.customInputDecorationName(),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
      ),
    )));
  }

  Container _buildAuthorField(TextEditingController authorController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: authorController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationAuthor(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildGenreField(TextEditingController genreController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: genreController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationGenre(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildWearField(TextEditingController wearController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: wearController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationWear(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildIsbnField(TextEditingController isbnController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: isbnController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationIsbn(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildDescriptionField(
      TextEditingController descriptionController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: descriptionController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationDescription(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildAddButton(BuildContext context, String Username) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
          style: Styles.raisedButtonStyle,
          onPressed: () async {
            if (nameController.text.isEmpty ||
                authorController.text.isEmpty ||
                genreController.text.isEmpty ||
                isbnController.text.isEmpty ||
                descriptionController.text.isEmpty ||
                wearController.text.isEmpty) {
              setState(() {
                changeErrVisibility("Visi laukai privalomi", true);
              });
            } else {
              var response = await http
                  .post(Uri.parse("http://23.97.221.80/Books/reg.php"), body: {
                "name": nameController.text,
                "author": authorController.text,
                "genre": genreController.text,
                "isbn": isbnController.text,
                "description": descriptionController.text,
                "wear": wearController.text,
                "user": Username
              });
              if (response.body == "1") {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeClass()),
                );
              } else {
                changeErrVisibility(response.body.toString(), true);
              }
            }
          },
          child: Text(
            'Pridėti',
            style: Styles.customButtonTextStyle(),
          )),
    );
  }

  TextStyle _customButtonTextStyle() {
    return const TextStyle(
      color: Color.fromARGB(255, 255, 255, 255),
      letterSpacing: 1.5,
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      fontFamily: 'OpenSans',
    );
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.black87,
    primary: Color.fromARGB(189, 47, 145, 190),
    minimumSize: const Size(88, 60),
    padding: const EdgeInsets.all(15.0),
  );

  InputDecoration _customInputDecorationUser() {
    return const InputDecoration(
      fillColor: Color.fromARGB(255, 255, 255, 255),
      filled: true,
      hintText: 'Vartotojo vardas',
      contentPadding: EdgeInsets.all(20.0),
      border: OutlineInputBorder(),
    );
  }

  InputDecoration _customInputDecorationPassword() {
    return const InputDecoration(
      fillColor: Color.fromARGB(255, 255, 255, 255),
      filled: true,
      hintText: 'Slaptažodis',
      contentPadding: EdgeInsets.all(20.0),
      border: OutlineInputBorder(),
    );
  }
}
