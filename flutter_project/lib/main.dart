import 'package:flutter/material.dart';
import 'loginScreen.dart';
import 'home.dart';

const String appName = 'Knygų klubas';

void main() {
  runApp(const MaterialApp(home: App(loggedIn: "0")));
}

class App extends StatelessWidget {
  const App({Key? key, required this.loggedIn}) : super(key: key);
  final String loggedIn;

  @override
  Widget build(BuildContext context) {
    if (loggedIn == "0") {
      return const MediaQuery(data: MediaQueryData(), child: LoginClass());
    } else {
      return const HomeClass();
    }
  }
}
