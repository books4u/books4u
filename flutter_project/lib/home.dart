import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_project/AddBookScreen.dart';

import 'loginScreen.dart';
import 'main.dart';
import 'styles.dart';
import 'bookClass.dart' as bookClass;
import 'profileClass.dart' as profileClass;
import 'package:http/http.dart' as http;
//Pages

class MyBooksPage extends StatefulWidget {
  const MyBooksPage({Key? key}) : super(key: key);
  @override
  State<MyBooksPage> createState() => MyBooksPageState();
}

class MyBooksPageState extends State<MyBooksPage> {
  @override
  void initState() {
    super.initState();
    Username = LoginClassState.userController.text;
    bookAlbum = bookClass.fetchMyBooks();
  }

  bool isMsgVisible = false;
  String msg = "";

  void changeErrVisibility(String code, bool change) {
    if (change) {
      setState(() {
        isMsgVisible = change;
        msg = "*" + code;
      });
    }
  }

  late Future<List<bookClass.Book>> bookAlbum;

  static String Username = "";

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Scaffold(
      body: Center(
        child: FutureBuilder<List<bookClass.Book>>(
          future: bookAlbum,
          builder: (context, snapshot) {
            // print(snapshot.data?.isbn);
            //print(snapshot.toString());

            if (snapshot.hasData) {
              print(snapshot.data);
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      title: Text(snapshot.data![index].name),
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                  content: Stack(
                                overflow: Overflow.visible,
                                children: <Widget>[
                                  Positioned(
                                    right: -40.0,
                                    top: -40.0,
                                    child: InkResponse(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: CircleAvatar(
                                        child: Icon(
                                          Icons.close,
                                          color: Styles
                                              .defaultColorScheme.background,
                                        ),
                                        backgroundColor:
                                            Styles.defaultColorScheme.primary,
                                      ),
                                    ),
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        ConstrainedBox(
                                            constraints: const BoxConstraints(
                                                maxWidth: 500),
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Flexible(
                                                      child: Text(
                                                    '"' +
                                                        snapshot
                                                            .data![index].name +
                                                        '"',
                                                    style: Styles
                                                        .customBookTextStyle(),
                                                    textAlign: TextAlign.center,
                                                  )),
                                                  const SizedBox(height: 20)
                                                ])),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Flexible(
                                                  child: Text(
                                                snapshot.data![index].author,
                                                style: Styles
                                                    .customBookTextStyle(),
                                                textAlign: TextAlign.center,
                                              )),
                                              const SizedBox(height: 20)
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text("ISBN: " +
                                                  snapshot.data![index].isbn),
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: const [
                                              SizedBox(
                                                height: 40,
                                              )
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Flexible(
                                                child: Text("Būklė: " +
                                                    snapshot.data![index].wear),
                                              ),
                                              const SizedBox(height: 40)
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  "Aprašymas: " +
                                                      snapshot.data![index]
                                                          .description,
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ]),
                                      ]),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          _buildEditButton(
                                              context, snapshot, index),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: const [
                                          SizedBox(height: 10),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          _buildRemoveButton(
                                              context, snapshot, index)
                                        ],
                                      )
                                    ],
                                  )
                                ],
                              ));
                            });
                      });
                },
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AddBookClass()),
          );
        },
        backgroundColor: Styles.defaultColorScheme.primary,
        child: const Icon(Icons.add),
      ),
    ));
  }

  Container _buildEditButton(BuildContext context,
      AsyncSnapshot<List<bookClass.Book>> snapshot, int index) {
    final TextEditingController nameController =
        TextEditingController(text: snapshot.data![index].name);
    final TextEditingController authorController =
        TextEditingController(text: snapshot.data![index].name);
    final TextEditingController genreController =
        TextEditingController(text: snapshot.data![index].genre);
    final TextEditingController isbnController =
        TextEditingController(text: snapshot.data![index].isbn);
    final TextEditingController descriptionController =
        TextEditingController(text: snapshot.data![index].description);
    final TextEditingController wearController =
        TextEditingController(text: snapshot.data![index].wear);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
        style: Styles.raisedButtonStyle,
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                    content:
                        Stack(overflow: Overflow.visible, children: <Widget>[
                  Container(
                      width: 450,
                      height: 400,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(children: [
                        Column(children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                  width: 350,
                                  height: 50,
                                  child: TextFormField(
                                    controller: nameController,
                                    textAlignVertical: TextAlignVertical.bottom,
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        Styles.customInputDecorationName(),
                                    style: const TextStyle(
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                )))
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [SizedBox(height: 20)]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                  width: 350,
                                  height: 50,
                                  child: TextFormField(
                                    controller: authorController,
                                    textAlignVertical: TextAlignVertical.bottom,
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        Styles.customInputDecorationAuthor(),
                                    style: const TextStyle(
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                )))
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [SizedBox(height: 20)]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                  width: 350,
                                  height: 50,
                                  child: TextFormField(
                                    controller: genreController,
                                    textAlignVertical: TextAlignVertical.bottom,
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        Styles.customInputDecorationGenre(),
                                    style: const TextStyle(
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                )))
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [SizedBox(height: 20)]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                  width: 350,
                                  height: 50,
                                  child: TextFormField(
                                    controller: wearController,
                                    textAlignVertical: TextAlignVertical.bottom,
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        Styles.customInputDecorationWear(),
                                    style: const TextStyle(
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                )))
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [SizedBox(height: 20)]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                  width: 350,
                                  height: 50,
                                  child: TextFormField(
                                    controller: descriptionController,
                                    textAlignVertical: TextAlignVertical.bottom,
                                    textAlign: TextAlign.left,
                                    keyboardType: TextInputType.text,
                                    decoration: Styles
                                        .customInputDecorationDescription(),
                                    style: const TextStyle(
                                        color: Color.fromARGB(255, 0, 0, 0)),
                                  ),
                                )))
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [SizedBox(height: 20)]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    child: Center(
                                        child: SizedBox(
                                            width: 350,
                                            height: 50,
                                            child: ElevatedButton(
                                              style: Styles.raisedButtonStyle,
                                              onPressed: () async {
                                                try {
                                                  var response = await http.post(
                                                      Uri.parse(
                                                          "http://23.97.221.80/Books/edit.php"),
                                                      body: {
                                                        "name":
                                                            nameController.text,
                                                        "author":
                                                            authorController
                                                                .text,
                                                        "wear":
                                                            wearController.text,
                                                        "description":
                                                            descriptionController
                                                                .text,
                                                        "isbn":
                                                            isbnController.text,
                                                        "genre":
                                                            genreController.text
                                                      });
                                                  if (response.statusCode ==
                                                      200) {
                                                    Navigator.pop(context);
                                                    Navigator.pop(context);
                                                    setState(() {
                                                      bookAlbum = bookClass
                                                          .fetchMyBooks();
                                                    });
                                                  }
                                                } catch (error) {
                                                  print("Server erroras");
                                                }
                                              },
                                              child: Text(
                                                'Taip',
                                                style: Styles
                                                    .customButtonTextStyle(),
                                              ),
                                            ))))
                              ]),
                        ])
                      ]))
                ]));
              });
        },
        child: Text(
          'Redaguoti',
          style: Styles.customButtonTextStyle(),
        ),
      ),
    );
  }

  Container _buildRemoveButton(BuildContext context,
      AsyncSnapshot<List<bookClass.Book>> snapshot, int index) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
        style: Styles.raisedButtonStyle,
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Container(
                        width: 250,
                        height: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Column(children: [
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text(
                                    "Ar tikrai norite šalinti šią knygą?",
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                              Row(
                                children: const [
                                  SizedBox(
                                    height: 25,
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          _buildYesButton(
                                              context, snapshot, index),
                                          const SizedBox(width: 85)
                                        ],
                                      )
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          _buildNoButton(
                                              context, snapshot, index)
                                        ],
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          )
                        ]),
                      ),
                      Positioned(
                        right: -40.0,
                        top: -40.0,
                        child: InkResponse(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: CircleAvatar(
                            child: Icon(
                              Icons.close,
                              color: Styles.defaultColorScheme.background,
                            ),
                            backgroundColor: Styles.defaultColorScheme.primary,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              });
        },
        child: Text(
          'Šalinti',
          style: Styles.customButtonTextStyle(),
        ),
      ),
    );
  }

  _buildNoButton(BuildContext context,
      AsyncSnapshot<List<bookClass.Book>> snapshot, int index) {
    return ElevatedButton(
      style: Styles.raisedButtonStyle,
      onPressed: () {
        Navigator.pop(context);
      },
      child: Text(
        'Ne',
        style: Styles.customButtonTextStyle(),
      ),
    );
  }

  Widget _buildYesButton(BuildContext context,
      AsyncSnapshot<List<bookClass.Book>> snapshot, int index) {
    return ElevatedButton(
      style: Styles.raisedButtonStyle,
      onPressed: () async {
        try {
          var response = await http.post(
              Uri.parse("http://23.97.221.80/Books/delete.php"),
              body: {"isbn": snapshot.data![index].isbn});
          if (response.statusCode == 200) {
            setState(() {
              bookAlbum = bookClass.fetchMyBooks();
            });
            Navigator.pop(context);
            Navigator.pop(context);
          }
        } catch (error) {
          print("Server erroras");
        }
      },
      child: Text(
        'Taip',
        style: Styles.customButtonTextStyle(),
      ),
    );
  }
}

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);
  @override
  State<ProfilePage> createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  late Future<List<profileClass.Profile>> profileAlbum;

  @override
  void initState() {
    super.initState();
    profileAlbum = profileClass.fetchProfile();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<profileClass.Profile>>(
        future: profileAlbum,
        builder: (context, snapshot) {
          // print(snapshot.data?.isbn);
          //print(snapshot.toString());

          if (snapshot.hasData) {
            return Center(
                child: Scaffold(
              body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                    Container(
                      padding: const EdgeInsets.fromLTRB(40, 40, 40, 40),
                      child: Text(snapshot.data![0].name,
                          style: const TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(snapshot.data![0].role,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    const SizedBox(
                      height: 200,
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(40, 40, 40, 40),
                      child: Text(snapshot.data![0].description,
                          style: const TextStyle(
                            fontSize: 20,
                          )),
                    ),
                  ])),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  onPressEdit(context, snapshot);
                },
                backgroundColor: Styles.defaultColorScheme.primary,
                child: const Icon(Icons.edit),
              ),
            ));
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }

          // By default, show a loading spinner.
          return const CircularProgressIndicator();
        });
  }

//
  onPressEdit(BuildContext context,
      AsyncSnapshot<List<profileClass.Profile>> snapshot) {
    final TextEditingController descrController =
        TextEditingController(text: snapshot.data![0].description);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: Stack(overflow: Overflow.visible, children: <Widget>[
            Container(
              width: 450,
              height: 130,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                child: Center(
                                    child: SizedBox(
                              width: 350,
                              height: 50,
                              child: TextFormField(
                                controller: descrController,
                                textAlignVertical: TextAlignVertical.bottom,
                                textAlign: TextAlign.left,
                                keyboardType: TextInputType.text,
                                decoration:
                                    Styles.customInputDecorationDescriptionP(),
                                style: const TextStyle(
                                    color: Color.fromARGB(255, 0, 0, 0)),
                              ),
                            )))
                          ]),
                    ],
                  ),
                  Row(
                    children: const [SizedBox(height: 20)],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          style: Styles.raisedButtonStyle,
                          onPressed: () async {
                            try {
                              var response = await http.post(
                                  Uri.parse(
                                      "http://23.97.221.80/UserAccounts/edit.php"),
                                  body: {
                                    "user": snapshot.data![0].name,
                                    "description": descrController.text,
                                  });
                              if (response.statusCode == 200) {
                                Navigator.pop(context);
                                setState(() {
                                  profileAlbum = profileClass.fetchProfile();
                                });
                              }
                            } catch (error) {
                              print("Server erroras");
                            }
                          },
                          child: Center(
                            child: Text(
                              'Taip',
                              style: Styles.customButtonTextStyle(),
                            ),
                          ))
                    ],
                  ),
                ],
              ),
            ),
          ]));
        });
  }
}

class HomeClass extends StatefulWidget {
  const HomeClass({Key? key}) : super(key: key);
  @override
  State<HomeClass> createState() => HomeClassState();
}

class HomeClassState extends State<HomeClass> {
  int selectedIndex = 0;

  static const TextStyle navigationTextStyle =
      TextStyle(fontSize: 38, fontWeight: FontWeight.bold);
  List<BottomNavigationBarItem> buildBottomNavBarItems() {
    return [
      const BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Visos knygos',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.category),
        label: 'Mano knygos',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.supervised_user_circle_rounded),
        label: 'Profilis',
      ),
    ];
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  Widget buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        pageChanged(index);
      },
      children: const <Widget>[
        MainPage(),
        MyBooksPage(),
        ProfilePage(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          colorScheme: Styles.defaultColorScheme,
        ),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color.fromARGB(195, 10, 10, 10),
            title: const Text(appName),
          ),
          body: buildPageView(),
          bottomNavigationBar: BottomNavigationBar(
              items: buildBottomNavBarItems(),
              currentIndex: selectedIndex,
              selectedItemColor: Styles.defaultColorScheme.primary,
              onTap: (index) {
                onNavigationItemTapped(index);
              }),
        ));
  }

  void pageChanged(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void onNavigationItemTapped(int index) {
    setState(() {
      selectedIndex = index;
      pageController.animateToPage(index,
          duration: const Duration(milliseconds: 500), curve: Curves.ease);
    });
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);
  @override
  State<MainPage> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  late Future<List<bookClass.Book>> bookAlbum;

  @override
  void initState() {
    super.initState();
    bookAlbum = bookClass.fetchBook();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder<List<bookClass.Book>>(
        future: bookAlbum,
        builder: (context, snapshot) {
          // print(snapshot.data?.isbn);
          //print(snapshot.toString());

          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                return ListTile(
                    title: Text(snapshot.data![index].name),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: Stack(
                                overflow: Overflow.visible,
                                children: <Widget>[
                                  Positioned(
                                    right: -40.0,
                                    top: -40.0,
                                    child: InkResponse(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: CircleAvatar(
                                        child: Icon(
                                          Icons.close,
                                          color: Styles
                                              .defaultColorScheme.background,
                                        ),
                                        backgroundColor:
                                            Styles.defaultColorScheme.primary,
                                      ),
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      ConstrainedBox(
                                          constraints: const BoxConstraints(
                                              maxWidth: 500),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Flexible(
                                                    child: Text(
                                                  '"' +
                                                      snapshot
                                                          .data![index].name +
                                                      '"',
                                                  style: Styles
                                                      .customBookTextStyle(),
                                                  textAlign: TextAlign.center,
                                                )),
                                                const SizedBox(height: 20)
                                              ])),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Flexible(
                                                child: Text(
                                              snapshot.data![index].author,
                                              style:
                                                  Styles.customBookTextStyle(),
                                              textAlign: TextAlign.center,
                                            )),
                                            const SizedBox(height: 20)
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("ISBN: " +
                                                snapshot.data![index].isbn),
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: const [
                                            SizedBox(
                                              height: 40,
                                            )
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: Text("Būklė: " +
                                                  snapshot.data![index].wear),
                                            ),
                                            const SizedBox(height: 40)
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: Text(
                                                "Aprašymas: " +
                                                    snapshot.data![index]
                                                        .description,
                                                textAlign: TextAlign.left,
                                              ),
                                            )
                                          ]),
                                    ],
                                  )
                                ],
                              ),
                            );
                          });
                    });
              },
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }

          // By default, show a loading spinner.
          return const CircularProgressIndicator();
        },
      ),
    );
  }
}
