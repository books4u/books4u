import 'package:flutter/material.dart';

class Styles {
  static TextStyle customButtonTextStyle() {
    return TextStyle(
      color: defaultColorScheme.secondary,
      letterSpacing: 1.5,
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      fontFamily: 'OpenSans',
    );
  }

  static TextStyle customBookTextStyle() {
    return TextStyle(
      color: defaultColorScheme.secondary,
      letterSpacing: 1,
      fontSize: 25,
      fontWeight: FontWeight.bold,
      fontFamily: 'OpenSans',
    );
  }

  static TextStyle customHintTextStyle() {
    return const TextStyle(
      color: Color.fromARGB(150, 12, 12, 12),
    );
  }

  static final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    primary: defaultColorScheme.primary,
    minimumSize: const Size(88, 60),
    padding: const EdgeInsets.all(20.0),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
  );

  static InputDecoration customInputDecorationUser() {
    return InputDecoration(
        prefixIcon: Icon(Icons.account_circle_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Vartotojo vardas',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationPassword() {
    return InputDecoration(
        prefixIcon: Icon(Icons.lock_outline_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Slaptažodis',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationName() {
    return InputDecoration(
        prefixIcon:
            Icon(Icons.book_rounded, color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Knygos pavadinimas',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationAuthor() {
    return InputDecoration(
        prefixIcon: Icon(Icons.account_circle_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Knygos autorius',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationIsbn() {
    return InputDecoration(
        prefixIcon: Icon(Icons.format_list_numbered_rtl_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Brūkšninis kodas',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationWear() {
    return InputDecoration(
        prefixIcon: Icon(Icons.bookmark, color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Knygos būklė',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationDescription() {
    return InputDecoration(
        prefixIcon: Icon(Icons.description_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Knygos aprašas',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationDescriptionP() {
    return InputDecoration(
        prefixIcon: Icon(Icons.description_rounded,
            color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Profilio aprašas',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static InputDecoration customInputDecorationGenre() {
    return InputDecoration(
        prefixIcon:
            Icon(Icons.book_rounded, color: defaultColorScheme.background),
        fillColor: defaultColorScheme.secondary,
        filled: true,
        hintText: 'Knygos žanras',
        hintStyle: customHintTextStyle(),
        contentPadding: const EdgeInsets.all(20.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 2.0),
        ));
  }

  static ColorScheme defaultColorScheme = const ColorScheme(
    primary: Color.fromARGB(255, 162, 112, 53),
    secondary: Color.fromARGB(255, 235, 235, 235),
    surface: Color(0xff181818),
    background: Color(0xff121212),
    error: Color.fromARGB(255, 177, 64, 85),
    onPrimary: Color(0xff000000),
    onSecondary: Color.fromARGB(255, 205, 205, 205),
    onSurface: Color.fromARGB(255, 205, 205, 205),
    onBackground: Color.fromARGB(255, 205, 205, 205),
    onError: Color(0xff000000),
    brightness: Brightness.dark,
  );
}
