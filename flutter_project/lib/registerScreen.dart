import 'package:flutter/material.dart';
import 'package:flutter_project/loginScreen.dart';
import 'package:http/http.dart' as http;
import 'styles.dart';

class RegisterClass extends StatefulWidget {
  const RegisterClass({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RegisterClassState();
}

class _RegisterClassState extends State<RegisterClass> {
  final TextEditingController userController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool isMsgVisible = false;
  String msg = "";

  void changeErrVisibility(String code, bool change) {
    if (change) {
      setState(() {
        isMsgVisible = change;
        msg = "*" + code;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          colorScheme: Styles.defaultColorScheme,
        ),
        home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: const Color.fromARGB(0, 0, 0, 0),
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back,
                  color: Styles.defaultColorScheme.secondary),
              onPressed: () => Navigator.pop(context, false),
            ),
          ),
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text('Books4u',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    )),
                const SizedBox(height: 40.0),
                _buildUserNameField(userController),
                const SizedBox(height: 20.0),
                _buildPasswordField(passwordController),
                const SizedBox(height: 10.0),
                Visibility(
                  child: Container(
                    width: 350,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      msg,
                      style: TextStyle(
                        color: Styles.defaultColorScheme.error,
                      ),
                    ),
                    height: 20,
                  ),
                  visible: isMsgVisible,
                ),
                Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      const SizedBox(
                          width: 350,
                          child: Divider(
                              color: Color.fromARGB(159, 131, 129, 129))),
                      _buildRegisterButton(context)
                    ]))
              ],
            ),
          ),
        ));
  }

  Container _buildPasswordField(TextEditingController passwordController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        obscureText: true,
        enableSuggestions: false,
        autocorrect: false,
        controller: passwordController,
        textAlignVertical: TextAlignVertical.bottom,
        textAlign: TextAlign.left,
        keyboardType: TextInputType.text,
        decoration: Styles.customInputDecorationPassword(),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
      ),
    )));
  }

  Container _buildUserNameField(TextEditingController userController) {
    return Container(
        child: Center(
            child: SizedBox(
      width: 350,
      height: 50,
      child: TextFormField(
        controller: userController,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: Styles.customInputDecorationUser(),
        textAlignVertical: TextAlignVertical.bottom,
        keyboardType: TextInputType.text,
      ),
    )));
  }

  Container _buildRegisterButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      width: 350,
      child: ElevatedButton(
          style: Styles.raisedButtonStyle,
          onPressed: () async {
            var response = await http.post(
                Uri.parse("http://23.97.221.80/UserAccounts/reg.php"),
                body: {
                  "username": userController.text,
                  "password": passwordController.text,
                });
            if (response.body == "1") {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const LoginClass()),
              );
            } else {
              changeErrVisibility(response.body.toString(), true);
            }
          },
          child: Text(
            'Registruotis',
            style: Styles.customButtonTextStyle(),
          )),
    );
  }

  TextStyle _customButtonTextStyle() {
    return const TextStyle(
      color: Color.fromARGB(255, 255, 255, 255),
      letterSpacing: 1.5,
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      fontFamily: 'OpenSans',
    );
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.black87,
    primary: Color.fromARGB(189, 47, 145, 190),
    minimumSize: const Size(88, 60),
    padding: const EdgeInsets.all(15.0),
  );

  InputDecoration _customInputDecorationUser() {
    return const InputDecoration(
      fillColor: Color.fromARGB(255, 255, 255, 255),
      filled: true,
      hintText: 'Vartotojo vardas',
      contentPadding: EdgeInsets.all(20.0),
      border: OutlineInputBorder(),
    );
  }

  InputDecoration _customInputDecorationPassword() {
    return const InputDecoration(
      fillColor: Color.fromARGB(255, 255, 255, 255),
      filled: true,
      hintText: 'Slaptažodis',
      contentPadding: EdgeInsets.all(20.0),
      border: OutlineInputBorder(),
    );
  }
}
